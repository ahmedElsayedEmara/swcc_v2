$(function () {

    "use stric";
    // Scroll Top 
    var theLanguage = $('body').css('direction');

    var scrollButton = $("#scroll-top");
    scrollButton.click(function () {
        $("html,body").animate({
            scrollTop: 0
        }, 1500);
    });

    // remove collapse in mobile
    $('.navbar.navbar-default .navs').click(function () {
        $('.navbar.navbar-default .navbar-collapse').removeClass('in');
        $('.navbar-header .navbar-toggle').removeClass('open');
    });

    // News Sldier
    if ($(".newSlider").length > 0) {
        $(".newSlider").owlCarousel({
            items: 1,
            autoplay: 4000,
            loop: true,
            nav: true,
            navText: ["<i class='fa fa-angle-right'></i>", "<i class='fa fa-angle-left'></i>"]
        });
    }

    // Offer Slider
    if ($(".offerSlider").length > 0) {
        $(".offerSlider").owlCarousel({
            items: 2,
            autoplay: 4000,
            loop: true,
            margin: 25,
            nav: true,
            navText: ["<i class='fa fa-angle-right'></i>", "<i class='fa fa-angle-left'></i>"],
            responsive: {
                0: {
                    items: 1
                },
                991: {
                    items: 2
                }
            }
        });
    }
 

    // SliderPro News 
    if ($('#sliderPro').length > 0) {
        $('#sliderPro').sliderPro({
            width: 'auto',
            height: '600',
            // autoHeight:true,
            orientation: 'vertical',
            loop: false,
            arrows: false,
            buttons: false,
            autoplay: true,
            thumbnailsPosition: 'left',
            thumbnailPointer: true,
            thumbnailWidth: 300,
            fade: true,
            thumbnailHeight: '233',
            breakpoints: {
                1000: {
                    thumbnailsPosition: 'bottom',
                    thumbnailWidth: 250,
                    thumbnailHeight: 300
                }
            }
        });
    }

    // Offer Slider
    if ($(".sliderNews").length > 0) {
        $(".sliderNews").owlCarousel({
            items: 1,
            autoplay: 4000,
            loop: true,
            nav: true,
            navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"],
        });
    }
    // Open menu
    console.log(theLanguage)
    var collapseNav = $('.collapseNav');
    $('#navIcon').click(function () {
        collapseNav.toggleClass('active');
        if (collapseNav.hasClass('active')) {
            $('body').css({
                'overflow-y': 'hidden'
            });
            $('.collapseNav.active').animate({
                left: 0
            });
            $('.over').addClass('active');
            $('#navIcon i').removeClass(' fa-list-ul').addClass('fa fa-close');
        } else {
            $('body').css({
                'overflow-y': 'auto'
            });
            $('.collapseNav').animate({
                left: '-400'
            });
            $('.over').removeClass('active');
            $('#navIcon i').removeClass('fa fa-close').addClass('fa fa-list-ul');
        }
    });
    $('.over').click(function () {
        $('body').css({
            'overflow-y': 'auto'
        });
        $('.collapseNav').animate({
            left: '-400'
        });
        $('.over').removeClass('active');
        collapseNav.removeClass('active');
        $('#navIcon i').removeClass('fa fa-close').addClass('fa fa-list-ul');

    });

    // accordion
    $('#accordionExample').on('show.bs.collapse', function (n) {
        $(n.target).siblings('.card-header').find('button .angleDown i').toggleClass('fa-angle-up fa-angle-down');
    });
    $('#accordionExample').on('hide.bs.collapse', function (n) {
        $(n.target).siblings('.card-header').find('button .angleDown i').toggleClass('fa-angle-down fa-angle-up');
    });

    // niceScroll
    if ($('.announcementsBlock .surveyBody,.surveyHome,.sp-thumbnails-container').length > 0) {
        $(".announcementsBlock .surveyBody,.surveyHome,.sp-thumbnails-container").niceScroll({
            autohidemode: false,
            cursorcolor: '#147ABD',
            cursorfixedheight: 100,
            railalign:"left"
        });
    }

    // date
    if ($('.data_1').length > 0) {
        var cal = $.calendars.instance("ummalqura", 'ar');
        $('.data_1').calendarsPicker({
            monthsToShow: [1, 1],
            showOtherMonths: true,
            calendar: cal,
            dateFormat: "dd/mm/yyyy",
            yearRange: "c-50:c+10"
        });
    }
});

// Loading
window.onload = function () {
    $('.preLoad').delay(100).fadeOut(500);
}